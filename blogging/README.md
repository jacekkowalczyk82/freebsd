# Blogging with jekyll now 

* https://howchoo.com/g/yzg0yjdmntl/how-to-blog-in-markdown-using-github-and-jekyll-now#fork-jekyll-now-and-rename-the-repository-yourgithubusernamegithubio
* https://github.com/barryclark/jekyll-now 
* https://pages.github.com/
* https://help.github.com/articles/using-jekyll-with-pages


## My Forked jekyll Now 

```

git clone https://github.com/jacekkowalczyk82/jekyll-now-page.git

```
* Edit post file _posts/2019-01-18-Hello-World.md, commit and push 

* I removed part (keep it here for later, just in case ):

```
Next you can update your site name, avatar and other options using the _config.yml file in the root of your repository (shown below).

![_config.yml]({{ site.baseurl }}/images/config.png)

The easiest way to make your first post is to edit this one. Go into /_posts/ and update the Hello World markdown file. For more instructions head over to the [Jekyll Now repository](https://github.com/barryclark/jekyll-now) on GitHub.

```

* Check web page https://jacekkowalczyk82.github.io/

## Starting from clean jekyll 

* Installing ruby and jekyll

```
sudo pkg install ruby ruby24-gems
gem install jekyll bundler 

```

* Adding new posts 

```

bundle exec jekyll post "Title of the new post "
bundle exec jekyll post "How to create a custom Debian ISO with DWM"
bundle exec jekyll post "How to convert markdown to html pdf pptx with pandoc" 

```
