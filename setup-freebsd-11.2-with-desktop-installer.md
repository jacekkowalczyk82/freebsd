# How to setup freebsd 11.2 with desktop installer 

* 1. Boot from Freebsd 11.2 dvd1 iso 
* 2. When asked for additional components select :
  * iports
  * sources
  * doc
  * lib32

* 3. Set hostname freebsd-11-dev
* 4. Set keyboard PL
* 5. Set timezone CET
* 6. In network configuration accept the default configs and cnage only the second DNS to 8.8.8.8 - google DNS
* 7. Services

```
local_unbound - NO - Local caching validating resolver 
sshd - YES
moused - NO - PS2 mouse pointer on console 
ntpd - YES 
powerd - NO - Adjust CPU frequency dynamically if supported 
dumpdev - YES - Enable Kernel crash dumps to /var/crash
```

* 8. System Hardening - nothing 

* 9. Add user kowalczy, with groups: kowalczy wheel operator and shell: tcsh 

* 10. Final Configuration - Install HandBook 


## FreeBSD 11.2 - Post installer configurations and steps

```
# as root 

freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 

pkg update
pkg upgrade

pkg install nano sudo vim git curl mc htop
 
nano /usr/local/etc/sudoers
uncomment line: %wheel  ALL = (ALL)  ALL


reboot

```

## Install desktop by desktop-installer

```
pkg install desktop-installer

# run it as root user by 
desktop-installer

```

* Switch to the latest binary packages instead of quarterly snapshots (y/n) [y] - y 
* Update installed packages [y]/n - enter
* Autoremove unneede packages? [y]/n - enter
* Update ports tree? [y]/n - enter
* Update base system ? [y]/n - enter
* No Updates needed to update system to 11.2-reaalease-p4, Reboot? y/[n] - enter 
* Build from source ? (y/n) [n] - enter
* Desktop selection 8-MATE 
* Reconfigure wireless networking? (y/n) [n] - enter
* Scan for additional sound devices? (y/[n]) - enter 
* Install Virtualbox guest addiotions? (y/n) [y] - enter
* Reconfigure X11? (y/n) [y] - enter 
* Use moused? (y/n) [n] - enter 
* Generate new xorg.conf? (y/n) [n] - enter
* Test X11 ? (y/n) [y] - enter
* Forward X11 DISPLAY to other hosts over ssh? (y/n) [y] - enter 
* Trust all forwarded X11 host? this is a security risk (y/n) [n] - enter
* Accept forwarded X11 DISPLAY from other hosts over ssh ? (y/n) [y] -enter 
* Enable SLIM graphiocal login? (y/n) [y] -  enter
* Login to GUI 
* Start new terminal emulator

```
cd /root
desktop-installer

```
* Go through the previous answered questions up to some new 
* Configure sudo for removable media mounting (y/n) [n] - y 
* Choosing flash player  -1 Gnash, default is 2 Linux Flash - I selected 1 Gnash 
* Install Java plugin (y/n) [n] - enter 
* Install security/keypassx2? (y/n) [n] - enter 
* Install sysutils/bsdstats ? (y/n) [y] - enter 
* Would you like to run BSDstats now [yes] ? - enter 
* Would you like to enable reporting on bootup in /etc/rc.conf [yes] - enter
* Reboot now ? (y/n) [n] - enter 
* 
* 


## Install fluxbox at the syetm with already installed MATE 

```
sudo pkg install fluxbox eterm xfe leafpad 

```
* Create .xinitrc  file with content 

```
Eterm & 
xfe & 
fluxbox exec


```
*  Edit file /usr/local/etc/slim.conf and set 

```
login_cmd           exec /bin/sh - ~/.xinitrc %session
```
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

 









 






























