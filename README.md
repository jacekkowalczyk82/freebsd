# My freebsd manual 

## Graphics setup for my Lenovo m92p

```
pciconf -vl
pciconf -vl | less

jacek@m92p:~ % sysctl -a | egrep -i 'hw.machine|hw.model|hw.ncpu'
hw.machine: amd64
hw.model: Intel(R) Core(TM) i5-3470T CPU @ 2.90GHz
hw.ncpu: 4
hw.machine_arch: amd64
jacek@m92p:~ % 

kldstat
```

I installed package libva-intel-driver.
I noticed that system is not crashing but is very sluggish. I checked another configuration with removed drm2.ko from kld_list
My system is running FreeBSD 12.1-RELEASE-p6
I have Intel(R) Core(TM) i5-4300U CPU @ 1.90GHz (2494.28-MHz K8-class CPU)
and Intel Haswell-ULT Integrated Graphics Controller.

Finally the solution is :
1. Install package drm-kmod and package drm-legacy-kmod was automatically removed.
2. Install package libva-intel-driver
3. Set kld_list in /etc/rc.conf :

```
Code:

kld_list="/boot/modules/i915kms.ko fuse"

```


## Audio and sound 

```
sudo sysrc sound_load=yes
sudo sysrc snd_hda_load=yes
sudo less /boot/loader.conf 
sudo less /etc/rc.conf
sudo pkg install xfce4-mixer
sudo pkg install mixertui

```

## Debian 11 Bullseye running in bhyve vm 

This steps are based on the tutorial: https://www.cyberciti.biz/faq/how-to-install-linux-vm-on-freebsd-using-bhyve-and-zfs/
but I made some small modifications. 

Other usefull links: 

* https://www.cyberciti.biz/faq/how-to-install-linux-vm-on-freebsd-using-bhyve-and-zfs/
* https://www.truenas.com/community/threads/quick-guide-to-install-debian-10-in-bhyve-with-the-serial-console.85424/
* https://forums.freebsd.org/threads/bhyve-and-debian.81602/


Create tap interface and loa necessary kernel modules

```

su - 

ifconfig tap0 create
# sysctl net.link.tap.up_on_open=1
net.link.tap.up_on_open: 0 -> 1
echo "net.link.tap.up_on_open=1" >> /etc/sysctl.conf

kldload vmm
kldload nmdm
```

Edit `/boot/loader.conf` file and set 

```
vmm_load="YES"
nmdm_load="YES"
if_tap_load="YES"
if_bridge_load="YES"
```

Create interfaces:

```
ifconfig bridge0 create
ifconfig bridge0 addm em0

ifconfig bridge0 addm em0 addm tap0
ifconfig bridge0 up

```

Edit `/etc/rc.conf` file and set : 

```
# byhve and jail settings bridges 
cloned_interfaces="bridge0 tap0"
ifconfig_bridge0_name="em0bridge"
ifconfig_em0bridge="addm em0 addm tap0 up"
```

Create ZFS file system for the new VM

```
zfs create -V60G -o volmode=dev zroot/debianvm

```

Reboot the system 

Get the debian installer:

```
mkdir -p ~/bhyve/debian-11/
cd ~/bhyve/debian-11/
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.1.0-amd64-netinst.iso

```

Install packages: 

```
sudo pkg install grub2-bhyve bhyve-firmware uefi-edk2-bhyve tightvnc 
```

Start installation 

```
bhyve -c 1 -m 2G -w -H \
-s 0,hostbridge \
-s 3,ahci-cd,/usr/home/jacek/bhyve/debian-11/debian-11.1.0-amd64-netinst.iso \
-s 4,virtio-blk,/dev/zvol/zroot/debianvm \
-s 5,virtio-net,tap0 \
-s 29,fbuf,tcp=0.0.0.0:5900,w=800,h=600,wait \
-s 30,xhci,tablet \
-s 31,lpc -l com1,stdio \
-l bootrom,/usr/local/share/uefi-firmware/BHYVE_UEFI.fd \
debianvm

# and in second terminal 

vncviewer localhost:5900

```

Use Graphical installer (for unknow reasons the tui installer did not create some boot files and I was not able to boot)

The original totorial has one step aboput copying the grubx64.efi file, in my case it was not needed as these files were already there. But just in case, just before finishing the installer, go back and start shell on the freshly installed system: 

```
mkdir /target/boot/efi/EFI/BOOT/
# copy file - workaround for bhyve grub package #
# Pay attention to destination file bootx64.efi #
cp /target/boot/efi/EFI/debian/grubx64.efi /target/boot/efi/EFI/BOOT/bootx64.efi
```


Destroy the previous instance of vm 

```
bhyvectl --destroy --vm=debianvm
```

Create new instance 

```
bhyve -c 2 -m 2G -w -H \
-s 0,hostbridge \
-s 4,virtio-blk,/dev/zvol/zroot/debianvm \
-s 5,virtio-net,tap0 \
-s 29,fbuf,tcp=0.0.0.0:5900,w=1024,h=768,wait \
-s 30,xhci,tablet \
-s 31,lpc -l com1,stdio \
-l bootrom,/usr/local/share/uefi-firmware/BHYVE_UEFI.fd \
debianvm
```

You can also create a startup script: 

```
#!/bin/sh
# Name: startdebianvm
# Purpose: Simple script to start my Debian 10 VM using bhyve on FreeBSD
# Author: Vivek Gite {https://www.cyberciti.biz} under GPL v2.x+
-------------------------------------------------------------------------
# Lazy failsafe (not needed but I will leave them here)
ifconfig tap0 create
ifconfig em0bridge addm tap0
if ! kldstat | grep -w vmm.ko 
then
    kldload -v vmm
fi
if ! kldstat | grep -w nmdm.ko
then
    kldload -v nmdm
fi
bhyve -c 2 -m 2G -w -H \
 -s 0,hostbridge \
 -s 4,virtio-blk,/dev/zvol/zroot/debianvm \
 -s 5,virtio-net,tap0 \
 -s 29,fbuf,tcp=0.0.0.0:5900,w=1024,h=768 \
 -s 30,xhci,tablet \
 -s 31,lpc -l com1,stdio \
 -l bootrom,/usr/local/share/uefi-firmware/BHYVE_UEFI.fd \
 debianvm

```


## FreeBSD 12.1 at thinkcenter m92p - Post installer configurations and steps

* Update the system and packages

```
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 
portsnap update

pkg update
pkg upgrade
```

* Install some base packages

```
pkg install nano sudo curl wget vim mc htop git rsync tmux neofetch

```

* Edit sudoers file `/usr/local/etc/sudoers` for example with `nano`

```
#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL
```

* Set tcsh as default user shell, tcsh has history of commands by default. tcsh handles mc better - related to mc subshell option, common.c: unimplemented subshell type 1

```
sudo chsh -s /bin/tcsh jacek

```

* Install X11 

```
pkg install xorg 
```

* Install ntfs support 

```
sudo pkg install fusefs-ntfs
sudo kldload fuse
sud sysrc kld_list+=fuse

```

* This machine has Intel GPU so I installed required driver packages:

```
sudo pkg install drm-kmod libva-intel-driver
```

* Edit file `/etc/rc.conf` and set kld_list

```
kld_list="/boot/modules/i915kms.ko fuse"

```

* Install DWM (window manager) and other GUI packages 

```
sudo pkg install sterm dmenu dwm nitrogen chromium sakura pcmanfm leafpad scrot  neofetch 

```

* Setup `.xinitrc`

```
## DWM 
setxkbmap pl
xrdb ~/.Xresources

exec nitrogen --restore &

 #Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`
 #Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `
 #while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` `uptime | sed 's/.*,//'`"; sleep 1; done &
while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `; Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`"; sleep 1 ; done &

#exec compton -b & 

exec dwm 2>&1 |tee -a /home/jacek/full-dwm.log

```

* Reboot the system `sudo reboot`
* Login and start X11 by `startx`
* use web browser to download some wallpapers and use nitrogen to set them 


## Mounting external USB disk 

* add user to group operator

```
sudo pw groupmod -n operator -M jacek
```

* add to `/etc/devfs.rules`

```
[system=10]
# Added by desktop-installer.
add path 'ugen*' mode 0660 group operator
# Added by desktop-installer.
add path 'cuaU*' mode 0660 group operator
# Added by desktop-installer.
add path 'uhid*' mode 0660 group operator
# Added by desktop-installer.
add path 'usbctl*' mode 0660 group operator
# Added by desktop-installer.
add path 'usb/*' mode 0660 group operator
# Added by desktop-installer.
add path 'video*' mode 0660 group operator
# Added by desktop-installer.
add path 'ng_ubt*' mode 0660 group operator
# Added by desktop-installer.
add path 'acd*' mode 0660 group operator
# Added by desktop-installer.
add path 'cd*' mode 0660 group operator
# Added by desktop-installer.
add path 'da*' mode 0660 group operator
# Added by desktop-installer.
add path 'pass*' mode 0660 group operator
# Added by desktop-installer.
add path 'xpt*' mode 0660 group operator
# Added by desktop-installer.
add path 'unlpt*' mode 0660 group cups
# Added by desktop-installer.
add path 'ulpt*' mode 0660 group cups
# Added by desktop-installer.
add path 'lpt*' mode 0660 group cups

```

* Enable the ruleset in /etc/rc.conf:

```
devfs_system_ruleset="localrules"
```

* Allow regular users to mount file systems by adding the following line to /etc/sysctl.conf:

```
vfs.usermount=1

```

* Create ZFS file system on new external disk 

```
#sudo mount -t msdosfs -o -m=644,-M=755 /dev/da0s1 /mnt/jacek/adata-2TB/
#sudo umount /mnt/jacek/adata-2TB/

sudo gpart destroy -F /dev/da0

sudo zpool create adata2TB /dev/da0
# /adata2TB will be mounted automatically on sytem boot 

sudo zfs create adata2TB/datasets 
sudo zfs create adata2TB/datasets/tomtom-with-compression
sudo zfs set compression=gzip adata2TB/datasets/tomtom-with-compression

sudo zfs create adata2TB/datasets/priv-with-compression
sudo zfs set compression=gzip adata2TB/datasets/priv-with-compression
sudo zfs set copies=2 adata2TB/datasets/priv-with-compression


# another 2TB disk drive da1 as blackAdata2TB

sudo gpart destroy -F /dev/da1

sudo zpool create blackAdata2TB /dev/da1
# /blackAdata2TB will be mounted automatically on sytem boot 

sudo zfs create blackAdata2TB/priv-no-compression
sudo zfs set copies=1 blackAdata2TB/priv-no-compression

sudo zfs create blackAdata2TB/priv-with-compression
sudo zfs set compression=gzip blackAdata2TB/priv-with-compression
sudo zfs set copies=2 blackAdata2TB/priv-with-compression

```


----

## FreeBSD upgrade to new release 12.1

```

sudo freebsd-update fetch
sudo freebsd-update install
sudo freebsd-update -r 12.1-RELEASE upgrade
sudo freebsd-update install

sudo reboot
sudo freebsd-update install


```

## Install base FreeBSD 12 in virtualbox (command line only )

**Installer Steps**

* Install
* keymap: Polish progremer's
* hostname: freebsd-12
* distribution select: lib32,ports,src,tests 
* file system: Auto ZFS 
* ZFS configuration: accept defaults and select Install
* ZFS configuration: Select stripe - no redundancy
* Select ada0 disk
* Set root password
* Select em0 interface
* Select DHCP for ip4
* Skip IPv6 configs
* Set IPv4 DNS #2 to google: 8.8.8.8
* Select Time zone region: Europe/Poland
* Set date and time 
* System configuration: enable services: ntpdate and ntpd
* System Hardening: nothing
* Add users: kowalczy, with additional groups wheel , default shell tcsh, 
* Additional configs: Install Handbook
* Exit installer
* Manual Configuration: No 
* Reboot 

## FreeBSD 12.0 at thinkpad x240 - Post installer configurations and steps

* Update the system and install basic apps as root user 

```
#updating the system and packages
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 
portsnap update


pkg update
pkg upgrade

#installing new packages
pkg install nano sudo curl wget vim mc htop git rsync
pkg install tmux neofetch

```

* Edit sdudoers file

```
nano /usr/local/etc/sudoers

#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL

```
* Reboot 

* Install GUI apps and configure the system 

```

pkg install xorg 
pkg install mate-desktop mate 

# run commands: 

sysrc dbus_enable=YES

cat /etc/rc.conf 


#edit fstab file to mount process file system
nano /etc/fstab 

#add this line to fstab file
proc        /proc       procfs  rw      0   0

reboot 

```

* Install Graphics drivers for 12.0
  * https://forums.freebsd.org/threads/how-to-use-the-old-or-the-new-i915kms-driver-for-intel-integrated-graphics-with-xorg.66732/
  * About Xorg-crash - https://forums.freebsd.org/threads/xorg-crash.70744/

```
#cd /usr/ports/graphics/drm-kmod/ 
#sudo make install clean
sudo pkg install drm-kmod
#enable loading of intel graphics kernel module at startup
sudo echo "# for graphics" >> /etc/rc.conf
sudo echo "kld_list=\"/boot/modules/i915kms.ko\ "" >> /etc/rc.conf 
#sudo echo "kld_list=\"i915kms\"" >> /etc/rc.conf

```

* Install NTFS support 

```
pkg install fusefs-ntfs
kldload fuse
sysrc kld_list+=fuse


```

* `/etc/rc.conf` kernel modules list 

```
kld_list="/boot/modules/i915kms.ko fuse "

```

* Get my configs and scripts 

```
mkdir -p ~/git/gitlab.com/
cd ~/git/gitlab.com/
git clone https://gitlab.com/jacekkowalczyk82/freebsd.git

cd freebsd 

#cp X11 starup scripts and xinitrc templates 
cp scripts/* ~/

```
* Install i3 and DWM 

```
# Installing i3wm  
#
pkg install i3 i3lock i3status 
pkg install dmenu nitrogen 

cp /usr/local/etc/i3/config ~/.config/i3/config 

#edit the file ~/.config/i3/config  and add at the beginning 
#use Windows key as mod key

set $mod Mod4 
# replace all default Mod1 with $mod 
# default Mod1 is left Alt key 


# Installing DWM from ports 
#
sudo pkg install sterm dmenu 
sudo pkg install surf-browser
sudo pkg install gcc libX11 libXft libXinerama 
cd /usr/ports/x11-wm/dwm
sudo make install clean 
sudo pkg lock dwm 

#select patches to apply: WinKey as ModKey and Volume Control (if running on laptop)

sudo make clean install


```

* Now I can start X session I want by running simple scripts:

```
./start-MATE.sh
./start-i3.sh
./start-DWM.sh

```

* Recommended readings: https://cooltrainer.org/a-freebsd-desktop-howto/
* https://cooltrainer.org/a-freebsd-desktop-howto/#desktop-environments


## Install KDE Plasma at fresh FreeBSD 12

```
# as root user 
#
#updating the system and packages
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 
portsnap update


pkg update
pkg upgrade


#installing new packages
pkg install nano sudo curl wget vim mc htop git 
pkg install tmux neofetch screenfech 

pkg install xorg 
pkg install kde5
pkg install kdevelop kate 
pkg install sddm 

sysrc dbus_enable=YES
sysrc sddm_enable=YES

cat /etc/rc.conf 

#edit /etc/sysctl.conf file 

net.local.stream.recvspace=65536
net.local.stream.sendspace=65536


#edit fstab file to mount process file system
nano /etc/fstab 

#add this line to fstab file
proc        /proc       procfs  rw      0   0

#edit rc.conf file to enable services
nano /etc/rc.conf 

reboot 


# Install virtual box guest additions 
# https://cooltrainer.org/a-freebsd-desktop-howto/#virtualization

pkg install virtualbox-ose-additions

#edit rc.conf file to enable autostart of virtualbox guest additions service 
nano /etc/rc.conf

#add these lines in rc.conf 
vboxguest_enable="YES"
vboxservice_enable="YES"


# SUDOERS
#
#edit sdudoers file
nano /usr/local/etc/sudoers

#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL



## Additional  applications

pkg install xedit geany ted scribus diffuse xterm roxterm terminator 
pkg install firefox seamonkey libreoffice gimp nitrogen scrot
pkg install gpicview evince galculator xarchiver mpv vlc audacious audacious-plugins 
pkg install bsdstats

```

## Install MATE and Lightweight Window Manager(s) at fresh FreeBSD 12

* Install XORG and MATE 

```
# as root user 
#
#updating the system and packages
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 
portsnap update

pkg update
pkg upgrade

#installing new packages
pkg install nano sudo curl wget vim mc htop git 
pkg install tmux neofetch

pkg install xorg 
pkg install mate-desktop mate 
pkg install sddm sddm-freebsd-black-theme

ls /usr/local/share/sddm/themes/

#Eddit the file `/usr/local/etc/sddm.conf` and set new value for CurrentTheme:

CurrentTheme=sddm-freebsd-black-theme

```

* Installing i3wm  

```
pkg install i3 i3lock i3status 
pkg install dmenu nitrogen 

cp /usr/local/etc/i3/config ~/.config/i3/config 

#edit the file ~/.config/i3/config  and add at the beginning 
#use Windows key as mod key

set $mod Mod4 
# replace all default Mod1 with $mod 
# default Mod1 is left Alt key 
```

* Installing DWM from ports 

```
sudo pkg install dwm sterm dmenu 
sudo pkg install surf-browser
sudo pkg install gcc libX11 libXft libXinerama 
cd /usr/ports/x11-wm/dwm
sudo make install clean 

#select patches to apply: WinKey as ModKey and Volume Control (if running on laptop)

sudo make clean install

# prepare .xinitrc file

# ## DWM 
setxkbmap pl
xrdb ~/.Xresources

exec xrandr --output VGA-0 --mode 1920x975 --rate 60 &
exec nitrogen --restore &
exec plank & 

#dwmstatus & 
#Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`
#Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `
#while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` `uptime | sed 's/.*,//'`"; sleep 1; done &

while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `; Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`"; sleep 1 ; done &

#if you have compton 
#compton -b --config ~/.config/compton.conf & 

exec dwm

#### end of .xinitrc file
 
 
#create a custom DWM xsession file `/usr/local/share/xsession/customDWM.desktop `

Encoding=UTF-8
Type=XSession 
Exec=/usr/local/share/sddm/scripts/xinit-session
TryExec=/usr/local/share/sddm/scripts/xinit-session
Name=Custom DWM 

```
* Configure services required for GUI

```
sysrc dbus_enable=YES
sysrc sddm_enable=YES

cat /etc/rc.conf 


#edit fstab file to mount process file system
nano /etc/fstab 

#add this line to fstab file
proc        /proc       procfs  rw      0   0

```

* reboot 

* Install virtual box guest additions 

```
pkg install virtualbox-ose-additions

#edit rc.conf file to enable autostart of virtualbox guest additions service 
nano /etc/rc.conf

#add these lines in rc.conf 
vboxguest_enable="YES"
vboxservice_enable="YES"
```

* Configure SUDOERS

```
#edit sdudoers file
nano /usr/local/etc/sudoers

#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL

```

* Install Additional  applications

```
pkg install xedit geany ted scribus diffuse xterm roxterm rxvt-unicode terminator 
pkg install firefox seamonkey libreoffice gimp nitrogen scrot
pkg install gpicview evince galculator xarchiver mpv vlc audacious 
pkg install bsdstats

```

* Reboot the computer and on the SDDM screen select the session you want to use and login 


## Simple login to X11 Session without SDDM or SLIM 

* Disable sddm or slim in `/etc/rc.conf` by commenting lines 

```
#sddm_enable="YES"
#slim_enable="YES"
```
* create shell scripts for starting xsessions, use the examples: [examples](./scripts/)

```
#!/bin/sh -x

SESSION=$1
cp xinitrc_${SESSION}.template .xinitrc

startx 
```

* Now you can start X session you want by running simple scripts:

```
./start-MATE.sh
./start-i3.sh
./start-DWM.sh


```

## Usefull commands:

```
#list connected usb and internal disk drives
camcontrol devlist


```

## Mounting USB drive FAT32 NTFS

```

dmesg
cat /etc/devfs.rules | grep da0
cat /etc/devfs.rules | grep da
mkdir usb-pendrive-mount
mount -t msdosfs -o -m=644,-M=755 /dev/da0 usb-pendrive-mount/

dmesg
#in the output I found : da0: <TOSHIBA External USB 3.0 0> Fixed Direct Access SPC-4 SCSI device

jacek@thinkpad-x240:~ %  file -s /dev/da0s1
/dev/da0s1: DOS/MBR boot sector, code offset 0x52+2, OEM-ID "NTFS    ", sectors/cluster 8, Media descriptor 0xf8, sectors/track 63, heads 255, hidden sectors 2048, dos < 4.0 BootSector (0x80), FAT (1Y bit by descriptor); NTFS, sectors/track 63, sectors 1953519615, $MFT start cluster 786432, $MFTMirror start cluster 16, bytes/RecordSegment 2^(-1*246), clusters/index block 1, serial number 0843671a53671993e; containsMicrosoft Windows XP/VISTA bootloader BOOTMGR

sudo pkg install fusefs-ntfs
sudo kldload fuse
sudo sysrc kld_list+=fuse

mkdir /mnt/usb-1TB-Toshiba-mount
#reconnect the disk drive
sudo ntfs-3g -o ro /dev/da0s1 /mnt/usb-1TB-Toshiba-mount/

sudo ntfs-3g -o rw /dev/da0s1 /mnt/usb-1TB-Toshiba-mount/

```
## My Favourite tools and applications

For any OS I always install tools like: nano, vim, git, htop, wget,curl, mc, neofetch (or screenfetch), 

* IDEs I already use:
  * geany
  * visual-studio-code
  * eclipse


* IDEs I need to learn more and start using:
  * kdevelop
    * additional packages: cmake, 
  * codelite
  ** emacs

* Markdown tools (packages installed): geany-plugin-markdown markdown py27-mdv mdp pandoc ghostwriter

## Installation FreeBSD 11.2

* Auto ZFS
* created user added to group wheel 

## FreeBSD 11.2  in Virtualbox VM - Post installer configurations and steps

```
# as root 

#updating the system and packages
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 

pkg update
pkg upgrade

#installing new packages
pkg install bash nano sudo 

#switching to bash shell 
bash 

#edit sdudoers file
nano /usr/local/utc/sudoers

#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL

# xf86-video-fbdev not supported yet, Some manuals were suggesting to install this, I did not installed this. 

#Installing xorg server and mate desktop  with some applications
pkg install xorg 
pkg install mate-desktop mate slim vim htop git screenfetch 

#edit fstab file to mount process file system
nano /etc/fstab 

#add this line to fstab file
proc        /proc       procfs  rw      0   0

#edit rc.conf file to enable services
nano /etc/rc.conf 

#add these 3 lines to rc.conf
dbus_enable="YES"
hald_enable="YES"
slim_enable="YES"


#enable autostart of the MATE desktop after login in SLIM
cd ~/
nano .xinitrc

#insert this line into ~/.xinitrc file
exec /usr/local/bin/mate-session

#create .xsession link to .xinitrc
ln -s .xinitrc .xsession

#copy .xinitrc to the user account home directory for user kowalczy
cp .xinitrc /home/kowalczy/
ln -s /home/kowalczy/.xinitrc /home/kowalczy/.xsession

chown kowalczy:kowalczy /home/kowalczy/.xinitrc
chown kowalczy:kowalczy /home/kowalczy/.xsession

#Install virtual box guest additions 
pkg install virtualbox-ose-additions


#edit rc.conf file to enable autostart of virtualbox guest additions service 
nano /etc/rc.conf

#add these lines in rc.conf 
vboxguest_enable="YES"
vboxservice_enable="YES"

#check version of FreeBSD system
freebsd-version 

#reboot system to start using it with MATE desktop
reboot 



```

## FreeBSD 11.2 at thinkpad x240 - Post installer configurations and steps

```
# as root 

#updating the system and packages
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 

pkg update
pkg upgrade

#installing new packages
pkg install bash nano sudo 

#switching to bash shell 
bash 

#edit sdudoers file
nano /usr/local/utc/sudoers

#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL


# xf86-video-fbdev not supported yet, Some manuals were suggesting to install this, I did not installed this. 

#Installing xorg server and mate desktop  with some applications
pkg install xorg 
pkg install mate-desktop mate slim vim htop git screenfetch 

#edit fstab file to mount process file system
nano /etc/fstab 

#add this line to fstab file
proc        /proc       procfs  rw      0   0

#edit rc.conf file to enable services
nano /etc/rc.conf 

#add these 3 lines to rc.conf
dbus_enable="YES"
hald_enable="YES"
slim_enable="YES"

#enable autostart of the MATE desktop after login in SLIM
cd ~/
nano .xinitrc

#insert this line into ~/.xinitrc file
exec /usr/local/bin/mate-session

#create .xsession link to .xinitrc
ln -s .xinitrc .xsession

#copy .xinitrc to the user account home directory for user kowalczy
cp .xinitrc /home/kowalczy/
ln -s /home/kowalczy/.xinitrc /home/kowalczy/.xsession

chown kowalczy:kowalczy /home/kowalczy/.xinitrc
chown kowalczy:kowalczy /home/kowalczy/.xsession

#check version of FreeBSD system
freebsd-version 

#reboot system to start using it with MATE desktop
reboot 


#At SLIM login screen login with your username and password

```

## Install Virtualbox at ThinkPad x240

* https://www.lenovo.com/us/en/laptops/thinkpad/x-series/x240/
* First of all in BIOS settings enable Kernel Virtualization of the CPU
* Install desired software

```
#check kernel version
freebsd-version -k

#install Subversion packages
sudo pkg install subversion

#checkout freebsd sources to /usr/src directory
sudo svn co svn://svn.freebsd.org/base/releng/11.2/ /usr/src

#update ports tree
portsnap fetch
portsnap update

#install gcc as it will be required for building packages
sudo pkg install gcc

#install virtualbox kernel modules 
cd /usr/ports/emulators/virtualbox-ose-kmod/
sudo make install clean

cd /usr/ports/emulators/virtualbox-ose
sudo make install clean

#add user jacek to groups vboxusers and operator
sudo pw groupmod vboxusers -m jacek
sudo pw groupmod operator -m jacek

#enable virtualbox networking module in rc.conf 

sudo pluma /etc/rc.conf

# add this to file
#for virtualbox
vboxnet_enable="YES"
devfs_system_ruleset="system"
 
#allow users from group vboxusers to use virtualbox networking file system 
sudo pluma /etc/devfs.conf 
#add this to file 
#for virtualbox 
own     vboxnetctl root:vboxusers
perm    vboxnetctl 0660

#load kernel module for virtualbox 
sudo kldload vboxdrv

#reload settings
sudo chown root:vboxusers /dev/vboxnetctl
sudo /etc/rc.d/devfs restart

#enable loading of virtualbox kernel module at startup 
sudo pluma /boot/loader.conf 

#add this to file 
#for virtualbox
vboxdrv_load="YES"


```

## Setup Wifi 

* Create file `/etc/wpa_supplicant.conf`

```
network={
    ssid="WORK_OFFICE_WIFI_SSID"
    psk="PIN_OR_PASSWORD"
    id_str="work"
}

network={
    ssid="HOME_WIFI_SSID"
    psk="PIN_OR_PASSWORD"
    id_str="home"
}


```
* restart networking by command

```
sudo service netif restart
```

## More responsive GUI 

* Edit file `/etc/sysctl.conf` and set 

```
kern.sched.preempt_thresh=224
```

* reboot machine 

## Other manuals resources 

* desktop-installer - https://www.youtube.com/watch?v=H7JVkqkke4c
* https://forums.freebsd.org/threads/using-freebsd-as-desktop-os.57329/
* https://antumdeluge.wordpress.com/2014/07/19/how-to-install-freebsd/
* https://cooltrainer.org/a-freebsd-desktop-howto/
* https://www.freebsdfoundation.org/freebsd/how-to-guides/installing-a-desktop-environment-on-freebsd/
* https://www.reddit.com/r/freebsd/comments/7dv7i2/main_reasons_why_you_choose_freebsd_as_a_desktop/
* http://www.osemotions.com/a-penguin-tries-freebsd-as-a-desktop-operating-system/
* https://sysconfig.org.uk/freebsd-on-the-desktop-am-i-crazy.html
* https://forums.freebsd.org/threads/howto-install-freebsd-desktop-version-for-newbies.59432/
* 
* 
* 
* 
* 

## Install Graphics drivers for 11.2

* https://forums.freebsd.org/threads/how-to-use-the-old-or-the-new-i915kms-driver-for-intel-integrated-graphics-with-xorg.66732/

```
cd /usr/ports/graphics/drm-next-kmod/ 
sudo make install clean

#enable loading of intel graphics kernel module at startup
sudo echo "# for graphics" >> /etc/rc.conf
sudo echo "kld_list=\"/boot/modules/i915kms.ko\"" >> /etc/rc.conf


```

## Connecting external display by the miniDP -> HDMI cable 

* Run xrandr without connected cable , and check available resolution modes and device names
* Connect cable and run xrandr command again. Examples: 

```
xrandr --output HDMI-1 --mode 1600x900 --above eDP-1 
# or 
xrandr --output HDMI-1 --mode 1600x900 --same-as eDP-1
# or just 
xrandr 

```

## Byobu 

```
sudo pkg install byobu
sudo mkdir -p /compat/linux/proc
sudo mount -t linprocfs linproc /compat/linux/proc
```
* To make it permanent, you need the following line in /etc/fstab:

```
linproc  /compat/linux/proc  linprocfs  rw,late  0 0

```

## FreeBSD 11.2 - install i3wm

```
# as root 

pkg update
pkg upgrade

pkg install i3 i3lock i3status 
pkg install dmenu nitrogen 


cp /usr/local/etc/i3/config ~/.config/i3/config 
edit the file ~/.config/i3/config  and add at the beginning 
#use Windows key as mod key
set $mod Mod4 
# replace all default Mod1 with $mod 
# default Mod1 is left Alt key 


nano ~/.xinitrc

# comment mate session line with # or remove previous call to start mate session and set 

/usr/local/bin/i3 

```

## Install Slim themes

```

sudo pkg install slim-themes
sudo nano /usr/local/etc/slim.conf

#set slim theme name by editing line with 'set current_theme'
set current_theme    fbsd

#the names of all themes can be found in /usr/local/share/slim/themes/
archlinux-simple
debian-moreblue
fbsd
flat
gentoo-simple
lake
mindlock
rainbow 
scotland-road 
wave
capernoited 
default 
fingerprint 
flower2 
gnewsense 
lunar 
parallel-dimensions 
rear-window 
subway 
zenwalk

```

## Update system and applications 

```
freebsd-update fetch
freebsd-update install

pkg update
pkg upgrade 

portsnap fetch
portsnap update

```

## Tweak system for Desktop use plus some usefull light GUI application

* Install packages:

```
sudo pkg install xedit geany ted scribus diffuse
sudo pkg install bsdstats

```

* Edit `/boot/loader.conf`  and add entries:

```
##### from desktop-installer investigation
kern.hz="100"
kern.maxfiles="25000"
kern.timecounter.hardware=i8254
uplcom_load="YES"
firewire_load="YES"
ng_ubt_load="YES"
kern.vty=vt
cuse4bsd_load="YES"

```

* Edit `/etc/fstab` and add lines:

```
# from desktop installer investigation , for automounting USB, cdrom devices
fdesc           /dev/fd                 fdescfs rw              0       0

/dev/cd0        /removable/cdrom0       cd9660  ro,noauto       0       0
/dev/cd1        /removable/cdrom1       cd9660  ro,noauto       0       0
/dev/cd2        /removable/cdrom2       cd9660  ro,noauto       0       0
/dev/da0s1      /removable/flash0       msdosfs rw,noauto       0       0
/dev/da1s1      /removable/flash1       msdosfs rw,noauto       0       0
/dev/da2s1      /removable/flash2       msdosfs rw,noauto       0       0


```

* Edit `/etc/devfs.conf` and add lines: 

```

# Added by desktop-installer:
perm    cuad0   0660
own     cuad0   root:operator

perm    cuad1   0660
own     cuad1   root:operator

perm    cuau0   0660
own     cuau0   root:operator

perm    cuau1   0660
own     cuau1   root:operator

perm    acd0    0660
own     acd0    root:operator

perm    acd1    0660
own     acd1    root:operator

perm    cd1     0660
own     cd1     root:operator

perm    fd0     0660
own     fd0     root:operator

perm    fd1     0660
own     fd1     root:operator

```

* Edit `/etc/devfs.rules` and add lines:

```

[system=10]
# Added by desktop-installer.
add path 'ugen*' mode 0660 group operator
# Added by desktop-installer.
add path 'cuaU*' mode 0660 group operator
# Added by desktop-installer.
add path 'uhid*' mode 0660 group operator
# Added by desktop-installer.
add path 'usbctl*' mode 0660 group operator
# Added by desktop-installer.
add path 'usb/*' mode 0660 group operator
# Added by desktop-installer.
add path 'video*' mode 0660 group operator
# Added by desktop-installer.
add path 'ng_ubt*' mode 0660 group operator
# Added by desktop-installer.
add path 'acd*' mode 0660 group operator
# Added by desktop-installer.
add path 'cd*' mode 0660 group operator
# Added by desktop-installer.
add path 'da*' mode 0660 group operator
# Added by desktop-installer.
add path 'pass*' mode 0660 group operator
# Added by desktop-installer.
add path 'xpt*' mode 0660 group operator
# Added by desktop-installer.
add path 'unlpt*' mode 0660 group cups
# Added by desktop-installer.
add path 'ulpt*' mode 0660 group cups
# Added by desktop-installer.
add path 'lpt*' mode 0660 group cups

```

* Edit `/etc/rc.conf` and add lines

```
# from desktop installer investigation
nfs_client_enable="YES"
#rpc_statd_enable="YES"
#rpc_lockd_enable="YES"
devd_enable="YES"
devfs_system_ruleset="system"

#vboxguest_enable="YES"
#vboxservice_enable="YES"
cupsd_enable="YES"
#webcamd_enable="YES"
bsdstats_enable="YES"


# from Robo Nuggie
#automount NFS
autofs_enable="YES"


```

* Edit `/etc/sysctl.conf` and add lines:

```

#more power for GUI
kern.sched.preempt_thresh=224

# From desktop-installer investigation
vfs.usermount=1
kern.ipc.shm_allow_removed=1


```

* Reboot the system 


## Install DWM (suckless.org) from ports as a desktop  - It is more stable than the latest one from git repository 

```
sudo pkg install dwm sterm dmenu 

sudo pkg install surf-browser

sudo pkg install gcc libX11 libXft libXinerama 
cd /usr/ports/x11-wm/dwm
sudo make install clean 

#select patches to apply: WinKey as ModKey and Volume Control (if running on laptop)

```

* `sudo make clean install`
* prepare xinitrc file

```
## DWM 
setxkbmap pl
xrdb ~/.Xresources

exec xrandr --output VGA-0 --mode 1920x975 --rate 60 &
exec nitrogen --restore &
exec plank & 

#dwmstatus & 
#Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`
#Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `
#while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` `uptime | sed 's/.*,//'`"; sleep 1; done &

while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `; Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`"; sleep 1 ; done &

exec dwm
 
 
```

## Install latest DWM from suckless.org as a desktop 

```
sudo pkg install dwm sterm dmenu 

sudo pkg install surf-browser

sudo pkg install gcc libX11 libXft libXinerama 
cd ~/git/
git clone git://git.suckless.org/dwm
```

* make changes in config.mk

```
diff --git a/config.mk b/config.mk
index 25e2685..b81970e 100644
--- a/config.mk
+++ b/config.mk
@@ -7,8 +7,8 @@ VERSION = 6.1
 PREFIX = /usr/local
 MANPREFIX = ${PREFIX}/share/man

-X11INC = /usr/X11R6/include
-X11LIB = /usr/X11R6/lib
+X11INC = /usr/local/include
+X11LIB = /usr/local/lib

 # Xinerama, comment if you don't want it
 XINERAMALIBS  = -lXinerama
@@ -16,7 +16,7 @@ XINERAMAFLAGS = -DXINERAMA

 # freetype
 FREETYPELIBS = -lfontconfig -lXft
-FREETYPEINC = /usr/include/freetype2
+FREETYPEINC = /usr/local/include/freetype2
 # OpenBSD (uncomment)
 #FREETYPEINC = ${X11INC}/freetype2

```

* make changes in config.def.h

```
diff --git a/config.def.h b/config.def.h
index 1c0b587..0bf6c2a 100644
--- a/config.def.h
+++ b/config.def.h
@@ -44,7 +44,7 @@ static const Layout layouts[] = {
 };

 /* key definitions */
-#define MODKEY Mod1Mask
+#define MODKEY Mod4Mask
 #define TAGKEYS(KEY,TAG) \
        { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
        { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \

```

* `sudo make clean install`
* prepare xinitrc file

```
## DWM 
setxkbmap pl
xrdb ~/.Xresources

exec xrandr --output VGA-0 --mode 1920x975 --rate 60 &
exec nitrogen --restore &
exec plank & 

#dwmstatus & 
#Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`
#Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `
#while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` `uptime | sed 's/.*,//'`"; sleep 1; done &

while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `; Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`"; sleep 1 ; done &

exec /home/kowalczy/git/dwm/dwm
 
 
```

* Edit `~/.Xresources` and set/add lines:

```
uxterm*background: black
uxterm*foreground: lightgray


XTerm.vt100.scrollBar: true
XTerm.vt100.scrollBar.width: 8

```

