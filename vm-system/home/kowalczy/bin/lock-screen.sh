#!/bin/sh 

LOCK_FILE=~/.fluxbox/backgrounds/lock-screen.png
CURRENT_XRANDR_RESOLUTION=$(xrandr |grep -m 1 "60.00\*+" |tail -n 1 |awk -F " " '{print $1}' |xargs -n 1 ) 
convert $LOCK_FILE -scale $CURRENT_XRANDR_RESOLUTION ${LOCK_FILE}_resized
i3lock -i ${LOCK_FILE}_resized


