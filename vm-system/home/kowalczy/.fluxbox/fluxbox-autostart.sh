#!/bin/bash 

#xrandr --output VGA-0 --mode 1920x975 --rate 60 

echo "DEBUG xrandr" > xrandr.log 
xrandr >> xrandr.log 
#echo "------" >> xrandr.log 
#xrandr |grep -m 3 60 >>  xrandr.log 
#echo "------" >> xrandr.log 
#xrandr |grep -m 3 60 |tail -n 1 |awk -F " " '{print $1}' |xargs -n 1 >>  xrandr.log 
#echo "------" >> xrandr.log 
BEST_XRANDR_RESOLUTION=$(xrandr |grep -m 1 "60.00" |tail -n 1 |awk -F " " '{print $1}' |xargs -n 1 ) 
xrandr -s ${BEST_XRANDR_RESOLUTION} 
echo "$BEST_XRANDR_RESOLUTION" >> xrandr.log 
echo "------" >> xrandr.log 

#fbsetbg -f /priv-data-mount/git/bitbucket.org/walpapers/freebsd/gO7kJ2m.jpg 
# set last background
#fbsetbg -l
# background is set by .fluxbox/backgrounds/init file
exec compton -b & 

exit 0 

