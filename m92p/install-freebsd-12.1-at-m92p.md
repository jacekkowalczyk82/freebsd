## Freebsd 12.1 install at m92p 


```
updating the system and packages
freebsd-update fetch 
freebsd-update install 

portsnap fetch 
portsnap extract 
portsnap update


pkg update
pkg upgrade

#installing new packages
pkg install nano 

pkg install sudo curl wget vim mc htop git rsync tmux neofetch

```

* pkg install output 

```
=====
Message from freetype2-2.10.2:

--
The 2.7.x series now uses the new subpixel hinting mode (V40 port's option) as
the default, emulating a modern version of ClearType. This change inevitably
leads to different rendering results, and you might change port's options to
adapt it to your taste (or use the new "FREETYPE_PROPERTIES" environment
variable).

The environment variable "FREETYPE_PROPERTIES" can be used to control the
driver properties. Example:

FREETYPE_PROPERTIES=truetype:interpreter-version=35 \
	cff:no-stem-darkening=1 \
	autofitter:warping=1

This allows to select, say, the subpixel hinting mode at runtime for a given
application.

If LONG_PCF_NAMES port's option was enabled, the PCF family names may include
the foundry and information whether they contain wide characters. For example,
"Sony Fixed" or "Misc Fixed Wide", instead of "Fixed". This can be disabled at
run time with using pcf:no-long-family-names property, if needed. Example:

FREETYPE_PROPERTIES=pcf:no-long-family-names=1

How to recreate fontconfig cache with using such environment variable,
if needed:
# env FREETYPE_PROPERTIES=pcf:no-long-family-names=1 fc-cache -fsv

The controllable properties are listed in the section "Controlling FreeType
Modules" in the reference's table of contents
(/usr/local/share/doc/freetype2/reference/site/index.html, if documentation was installed).
=====
Message from python37-3.7.9:

--
Note that some standard Python modules are provided as separate ports
as they require additional dependencies. They are available as:

py37-gdbm       databases/py-gdbm@py37
py37-sqlite3    databases/py-sqlite3@py37
py37-tkinter    x11-toolkits/py-tkinter@py37
=====
Message from ca_root_nss-3.56:

--
FreeBSD does not, and can not warrant that the certification authorities
whose certificates are included in this package have in any way been
audited for trustworthiness or RFC 3647 compliance.

Assessment and verification of trust is the complete responsibility of the
system administrator.


This package installs symlinks to support root certificates discovery by
default for software that uses OpenSSL.

This enables SSL Certificate Verification by client software without manual
intervention.

If you prefer to do this manually, replace the following symlinks with
either an empty file or your site-local certificate bundle.

  * /etc/ssl/cert.pem
  * /usr/local/etc/ssl/cert.pem
  * /usr/local/openssl/cert.pem
=====
Message from dejavu-2.37_1:

--
Make sure that the freetype module is loaded.  If it is not, add the following
line to the "Modules" section of your X Windows configuration file:

	Load "freetype"

Add the following line to the "Files" section of X Windows configuration file:

	FontPath "/usr/local/share/fonts/dejavu/"

Note: your X Windows configuration file is typically /etc/X11/XF86Config
if you are using XFree86, and /etc/X11/xorg.conf if you are using X.Org.
=====
Message from trousers-0.3.14_3:

--
To run tcsd automatically, add the following line to /etc/rc.conf:

tcsd_enable="YES"

You might want to edit /usr/local/etc/tcsd.conf to reflect your setup.

If you want to use tcsd with software TPM emulator, use the following
configuration in /etc/rc.conf:

tcsd_enable="YES"
tcsd_mode="emulator"
tpmd_enable="YES"

To use TPM, add your_account to '_tss' group like following:

# pw groupmod _tss -m your_account
=====
Message from perl5-5.30.3:

--
The /usr/bin/perl symlink has been removed starting with Perl 5.20.
For shebangs, you should either use:

#!/usr/local/bin/perl

or

#!/usr/bin/env perl

The first one will only work if you have a /usr/local/bin/perl,
the second will work as long as perl is in PATH.
=====
Message from libxkbcommon-0.10.0_2:

--
If arrow keys don't work under X11 switch to legacy rules e.g.,

For sh/bash/ksh/zsh run and (optionally) add into ~/.profile:
  export XKB_DEFAULT_RULES=xorg

For csh/tcsh run and (optionally) add into ~/.login:
  setenv XKB_DEFAULT_RULES xorg
=====
Message from spidermonkey60-60.9.0_3:

--
===>   NOTICE:

This port is deprecated; you may wish to reconsider installing it:

Uses Python 2.7 which is EOLed upstream.

It is scheduled to be removed on or after 2020-12-31.
=====
Message from apr-1.7.0.1.6.1_1:

--
The Apache Portable Runtime project removed support for FreeTDS with
version 1.6. Users requiring MS-SQL connectivity must migrate
configurations to use the added ODBC driver and FreeTDS' ODBC features.
=====
Message from gnupg-2.2.23:

--
GnuPG, when run on hosts without IPv6 connectivity, may fail to connect to
dual-stack hkp servers [1].  As a workaround, add

disable-ipv6

to

/usr/local/etc/dirmngr.conf

[1] https://dev.gnupg.org/rGecfc4db3a2f8bc2652ba4ac4de5ca1cd13bfcbec
=====
Message from libsigsegv-2.12:

--
Note that the stackoverflow handling functions of this library need
procfs mounted on /proc.
=====
Message from ruby-2.6.6,1:

--
Some of the standard commands are provided as separate ports for ease
of upgrading:

	devel/ruby-gems:	gem - RubyGems package manager
	devel/rubygem-irb:	irb - Interactive Ruby
	devel/rubygem-rake:	rake - Ruby Make
	devel/rubygem-rdoc:	rdoc - Ruby Documentation System

And some of the standard libraries are provided as separate ports
since they require extra dependencies:

	databases/rubygem-dbm:	DBM module
	databases/rubygem-gdbm:	GDBM module

Install them as occasion demands.
=====
Message from ctags-5.8:

--
The executable for Exuberant CTAGS is installed as /usr/local/bin/exctags
=====
Message from cscope-15.8b_1:

--
===>   NOTICE:

The cscope port currently does not have a maintainer. As a result, it is
more likely to have unresolved issues, not be up-to-date, or even be removed in
the future. To volunteer to maintain this port, please create an issue at:

https://bugs.freebsd.org/bugzilla

More information about port maintainership is available at:

https://www.freebsd.org/doc/en/articles/contributing/ports-contributing.html#maintain-port
=====
Message from lsof-4.93.2_11,8:

--
NOTE: Due to the way the FreeBSD build clusters make packages,
you may see a warning similar to the following:

lsof: WARNING: compiled for FreeBSD release 11.1-RELEASE-p6; this is 11.1-RELEASE-p4.

This is because the poudriere jails that build the packages don't have the  
corresponding kernel installed, so they don't know that the kernel version is different.

This warning can be ignored if you are using a system that is updated via freebsd-update
and are using pre-built packages.
=====
Message from git-2.27.0:

--
If you installed the GITWEB option please follow these instructions:

In the directory /usr/local/share/examples/git/gitweb you can find all files to
make gitweb work as a public repository on the web.

All you have to do to make gitweb work is:
1) Please be sure you're able to execute CGI scripts in
   /usr/local/share/examples/git/gitweb.
2) Set the GITWEB_CONFIG variable in your webserver's config to
   /usr/local/etc/git/gitweb.conf. This variable is passed to gitweb.cgi.
3) Restart server.


If you installed the CONTRIB option please note that the scripts are
installed in /usr/local/share/git-core/contrib. Some of them require
other ports to be installed (perl, python, etc), which you may need to
install manually.


```


* Edit sudoers file `/usr/local/etc/sudoers` for example with `nano`

```
#inside sudoers file, uncomment line: 
%wheel  ALL = (ALL)  ALL
```

* Set tcsh as default user shell, tcsh has history of commands by default. tcsh handles mc better - related to mc subshell option, common.c: unimplemented subshell type 1

```
sudo chsh -s /bin/tcsh jacek

```


* Install X11 

```
pkg install xorg 
```

* Message from xorg-server-1.20.8_4,1:
                                                                                                                                    
```                                                                                                                                                                        
--                                                                                                                                                                      
Xorg-server has been installed.

If your kernel is compiled with the EVDEV_SUPPORT option enabled
(default starting from FreeBSD 12.1) it is recommended to enable evdev mode in
pointer device drivers like ums(4) and psm(4). This will give improvements like
better tilt wheel support for mice and centralized gesture support via
xf86-input-synaptics or libinput drivers for touchpads.

This is also needed for PS/2 devices to be properly detected by Xorg when
moused service is disabled in /etc/rc.conf and kernel is compiled with
EVDEV_SUPPORT.

To enable evdev in such a device, run the following:

# sysctl kern.evdev.rcpt_mask=6

To make it persistent across reboots, add the following to /etc/sysctl.conf:

kern.evdev.rcpt_mask=6

In case you're using a serial mouse or any other mouse that *only* works over
sysmouse(4) and moused(8) on an evdev enabled kernel, please run this:

# sysctl kern.evdev.rcpt_mask=3

To make it persistent across reboots, add to this /etc/sysctl.conf:

kern.evdev.rcpt_mask=3
=====
Message from xterm-356:

--
You installed xterm with wide chars support. This introduces some limitations
comparing to the plain single chars version: this version of xterm will use
UTF-8 charset for selection buffers, breaking 8-bit copy/paste support unless
you are using UTF-8 or ISO8859-1 locale. If you want 8-bit charset selections to
work as before, use "eightBitSelectTypes" XTerm resource setting.

For further information refer to the SELECT/PASTE section of xterm(1) manual
page.

```

* Install ntfs support 

```
sudo pkg install fusefs-ntfs
sudo kldload fuse
sud sysrc kld_list+=fuse

```

* This machine has Intel GPU so I installed required driver packages:

```
sudo pkg install drm-kmod libva-intel-driver
```

* Edit file `/etc/rc.conf` and set kld_list

```
kld_list="/boot/modules/i915kms.ko fuse"

```


* Output from pkg: 

```

jacek@m92p:~ % sudo pkg install drm-kmod 
Updating FreeBSD repository catalogue...
FreeBSD repository is up to date.
All repositories are up to date.
The following 3 package(s) will be affected (of 0 checked):

New packages to be INSTALLED:
	drm-fbsd12.0-kmod: 4.16.g20200221
	drm-kmod: g20190710
	gpu-firmware-kmod: g20200503

Number of packages to be installed: 3

The process will require 51 MiB more space.
6 MiB to be downloaded.

Proceed with this action? [y/N]: y
[1/3] Fetching drm-kmod-g20190710.txz: 100%    812 B   0.8kB/s    00:01    
[2/3] Fetching drm-fbsd12.0-kmod-4.16.g20200221.txz: 100%    2 MiB   2.1MB/s    00:01    
[3/3] Fetching gpu-firmware-kmod-g20200503.txz: 100%    4 MiB   2.3MB/s    00:02    
Checking integrity... done (0 conflicting)
[1/3] Installing gpu-firmware-kmod-g20200503...
[1/3] Extracting gpu-firmware-kmod-g20200503: 100%
[2/3] Installing drm-fbsd12.0-kmod-4.16.g20200221...
[2/3] Extracting drm-fbsd12.0-kmod-4.16.g20200221: 100%
[3/3] Installing drm-kmod-g20190710...
=====
Message from drm-fbsd12.0-kmod-4.16.g20200221:

--
The drm-fbsd12.0-kmod port can be enabled for amdgpu (for AMD GPUs starting
with the HD7000 series / Tahiti) or i915kms (for Intel APUs starting with
HD3000 / Sandy Bridge) through kld_list in /etc/rc.conf. radeonkms for older
AMD GPUs can be loaded and there are some positive reports if EFI boot is NOT
enabled (similar to amdgpu).

For amdgpu: kld_list="amdgpu"
For Intel: kld_list="/boot/modules/i915kms.ko"
For radeonkms: kld_list="/boot/modules/radeonkms.ko"

Please ensure that all users requiring graphics are members of the
"video" group.

Older generations are supported by the legacy kms modules (radeonkms / 
i915kms) in base or by installing graphics/drm-legacy-kmod.

```

* Install DWM (window manager) and other GUI packages 

```
sudo pkg install sterm dmenu dwm nitrogen chromium sakura pcmanfm leafpad scrot  neofetch 

```

* setup `.xinitrc`

```
## DWM 
setxkbmap pl
xrdb ~/.Xresources

exec nitrogen --restore &

 #Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`
 #Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `
 #while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` `uptime | sed 's/.*,//'`"; sleep 1; done &
while true ; do xsetroot -name "`date '+%Y-%m-%d %H:%M.%S'` Load15: `uptime |awk -F "load" '{print $2}' |cut -d " " -f 5 `; Up: `uptime |cut -d "," -f 1 |awk -F "up" '{print $2}'`"; sleep 1 ; done &

#exec compton -b & 

exec dwm 2>&1 |tee -a /home/jacek/full-dwm.log

```

* Reboot the system `sudo reboot`
* Login and start X11 by `startx`

* use web browser to download some wallpapers and use nitrogen to set them 

