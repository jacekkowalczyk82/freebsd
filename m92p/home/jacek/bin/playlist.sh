#!/usr/bin/env bash
##!/usr/local/bin/bash


##!/usr/bin/env bash

PLAYLIST_FILE=~/.playtube-list.txt
if [[ ! -e $PLAYLIST_FILE ]]; then 
    echo "Error: Missing $PLAYLIST_FILE file"
else 
    echo "Playing music from list"
    cat $PLAYLIST_FILE
fi 


n=1
while read line; do
    # reading each line
    echo "Line No. $n : $line"
    n=$((n+1))
    if [[ "$line" != "" ]]; then 
        playtube.sh "$line"
        #echo "playing $line"
	sleep 1
         
    fi
done < $PLAYLIST_FILE

#echo "pause" > /tmp/mplayer-control
#echo "quit" > /tmp/mplayer-control

