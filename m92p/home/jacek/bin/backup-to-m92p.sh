#!/bin/sh 

BACKUP_TIMESTAMP="$(date '+week_%V__%Y-%m-%d-%H%M%S')"
LOG="/home/kowalczy/Documents/__LOG__home-backup.sh-${BACKUP_TIMESTAMP}.log"

echo "Backup files and folders $(date '+week_%V__%Y-%m-%d-%H%M%S') " 
#>> $LOG 2>&1

mkdir -p ~/linux_iso 
mv -v ~/Downloads/*.iso ~/linux_iso 

mkdir -p /adata2TB/datasets/priv-with-compression/`hostname`
rsync  -avz --progress  --exclude=jacek/.cache \
 --exclude=jacek/Music \
 --exclude=jacek/muza-kopia-z-adata \
 --exclude=jacek/mp3/music \
 --exclude=jacek/webm-music \
 --exclude="jacek/VirtualBox VMs" \
 /home/jacek /adata2TB/datasets/priv-with-compression/`hostname`/ 
#>> $LOG 2>&1



mkdir -p /adata2TB/datasets/priv-with-compression/linux_iso/
rsync  -avz --progress /home/jacek/linux_iso /adata2TB/datasets/priv-with-compression/linux_iso/ 
#>> $LOG 2>&1




echo "DONE $(date '+week_%V__%Y-%m-%d-%H%M%S')" 

