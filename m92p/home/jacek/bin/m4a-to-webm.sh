#!/bin/sh

find . -name "*.m4a" -exec ffmpeg -i '{}' '{}.webm' \;
find . -name "*.m4a.webm" -exec mv '{}' ~/webm-music/ \;

