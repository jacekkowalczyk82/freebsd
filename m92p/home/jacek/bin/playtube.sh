#!/usr/bin/env bash

##!/usr/local/bin/bash
##!/usr/bin/env bash

VIDEO_URL=$1
#https://www.youtube.com/watch?v=mlgR3PFq4_c

if [[ -z $VIDEO_URL ]]; then 
    echo "ERROR: Missing VIDEO_URL param "

    exit 1
fi 

echo "Getting audio music from $VIDEO_URL"

mkdir -p ~/private/music/youtube-dl-temp
cd ~/private/music/youtube-dl-temp

FILE_NAME=$(youtube-dl -f 251 --get-filename --restrict-filenames $VIDEO_URL )
echo "Saving audio file: $FILE_NAME"

if [[ -e $FILE_NAME ]]; then 
    echo "file $FILE_NAME exists"
else 
    youtube-dl -f 251 -o $FILE_NAME $VIDEO_URL
fi 


mkfifo /tmp/mplayer-control
mplayer -slave -input file=/tmp/mplayer-control -novideo $FILE_NAME

#echo "pause" > /tmp/mplayer-control
#echo "quit" > /tmp/mplayer-control
