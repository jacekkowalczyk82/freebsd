#!/usr/bin/env bash

##!/usr/local/bin/bash


PLAYLIST_FILE=~/.playtube-list.txt
if [[ ! -e $PLAYLIST_FILE ]]; then 
    echo "Error: Missing $PLAYLIST_FILE file"
else 
    echo "Playing music from list"
    cat $PLAYLIST_FILE
fi 


n=1
while read line; do
    # reading each line
    echo "Line No. $n : $line"
    n=$((n+1))
    if [[ "$line" != "" ]]; then 
        # playtube "$line"
        
        echo "Getting audio music from $line"
    
        mkdir -p ~/private/music/youtube-dl-temp
        cd ~/private/music/youtube-dl-temp

        FILE_NAME=$(youtube-dl -f 251 --get-filename --restrict-filenames $line )
        echo "Saving audio file: $FILE_NAME"

        if [[ -e $FILE_NAME ]]; then 
            echo "file $FILE_NAME exists"
        else 
            youtube-dl -f 251 -o $FILE_NAME $line
        fi 

        mkfifo /tmp/mplayer-control
        mplayer -slave -input file=/tmp/mplayer-control -novideo $FILE_NAME
        
        #echo "pause" > /tmp/mplayer-control
        #echo "quit" > /tmp/mplayer-control


    fi
done < $PLAYLIST_FILE

