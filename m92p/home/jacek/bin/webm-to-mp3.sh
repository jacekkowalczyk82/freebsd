#!/bin/sh

mkdir -p ~/mp3-music/
THIS_CONVERSION_DIR=converted_`date '+%Y-%m-%d_%H.%M.%S'`
mkdir -p ~/mp3-music/$THIS_CONVERSION_DIR

find . -name "*.webm" -exec ffmpeg -i '{}' '{}.mp3' \;
find . -name "*.webm.mp3" -exec mv '{}' ~/mp3-music/$THIS_CONVERSION_DIR/ \;

