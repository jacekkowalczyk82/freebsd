#!/bin/sh 

USB_BT_DEVICE=""

echo "==========================================="
while [ "$USB_BT_DEVICE" == "" ] ; do
echo "Select USB BT dongle device:"
echo -e "\t 0 - ubt0"
echo -e "\t 1 - ubt1"
echo -e "\t 2 - ubt2"
echo -e "\t q|Q - exit "
echo ""

read -p "Please select USB BT dongle device: " choice
case $choice in
  0) USB_BT_DEVICE=ubt0; break;;
  1) USB_BT_DEVICE=ubt1; break;;
  2) USB_BT_DEVICE=ubt2; break;;
  q|Q) exit 0;;
  *) echo "Invalid answer, please try again";;
#
esac
done

echo "==========================================="



# start Bt stack
echo "service bluetooth stop $USB_BT_DEVICE "
service bluetooth stop $USB_BT_DEVICE 

read -p "Please press any key to exit " any_key

