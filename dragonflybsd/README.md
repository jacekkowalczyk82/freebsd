# Install DragonFly  BSD 5.4 

* https://www.youtube.com/watch?v=tPSP_9cNgOc
* https://www.youtube.com/watch?v=S0-htW1wNjE
* https://www.youtube.com/watch?v=OP0jgxIiIJ0

## Basic system install in virtualbox 

* Boot ISO
* Login as installer 
* Select Install DragonFly BSD 
* Select Legacy BIOS
* Select ad0 disk
* Select Use Entire Disk 
* Select file system: Use UFS
* Create Subpartitions: Accept defaults and select: Accept and Create 
* Install OS: Select Begin Installing Files
* Install Bootblock(s): Select Accept and Install Bootblocks
* Select Configure this System
* Select timezone: UTC yes, Europe, Warsaw
* Set date and time
* Set keyboard map: PL 
* Set root password: 
* Add a user: kowalczy, other groups wheel
* Configure networrk interfaces: om0, use DHCP, 
* Configure hostname and domain
* Set Console font - nothing just return to Utilities menu
* Set screen map  - nothing just return to Utilities menu
* Return to Welcome Menu 
* Select Reboot this Computer

## Installing Xorg and apps

* Login as kowalczy  and run su to switch to root user
* Run commands 


```
pkg update
pkg upgrade
pkg install sudo htop git neofetch curl wget vim nano 
pkg install xorg 
pkg install xfce


pkg install slim slim-themes



pkg install firefox libreoffice gimp seamonkey geany xedit diffuse tmux nitrogen 
pkg install ted scribus gpicview evince galculator xarchiver mpv vlc audacious 

pkg install dmenu dwm sterm surf-browser slock screenfetch 

```

* Enable sudo by editing '/usr/local/etc/sudoers' file , uncomment line: "%wheel ALL=(ALL) ALL"

* Reboot 

* Edit /etc/rc.conf file and add 

```
moused_enable="YES"
dbus_enable="YES" 
hald_enable="YES"
slim_enable="YES"


```

* Edit ~/.xinitrc 

```
exec startxfce4 
```

* Create .xsession link to .xinitrc 

```
ln -s .xinitrc .xsession 
```

* Reboot 

* Changing slim theme 

```
# list installed slim themes
ls -al /usr/local/share/slim/themes 
```

* Edit slim config in '/usr/local/etc/slim.conf':

```
#set slim theme name by editing line with 'current_theme'
current_theme    flat

```
* Reboot 


## Remove XFCE and install MATE 

![Mate session desktop](./dragonflybsd/2018-12-12-151955_1024x768_scrot.png)

* I do not know why I cannot Shutdown or reboot the computer from XFCE session 
* Hmmm ... lets install MATE and remove XFCE 

```
pkg remove xfce 
pkg autoremove 

pkg install mate-desktop mate-base mate-common mate-terminal caja pluma mate-polkit mate-menus mate-panel mate-icon-theme 
pkg install scrot 

```
* Edit ~/.xinitrc 

```
exec mate-session
```

* I do not know why I cannot Shutdown or reboot the computer from MATE session too. 
* I will sisable slim and use `startx` to start GUI sessions
* Edit /etc/rc.conf file and comment slim_enable="YES" by # character at the beginning of the line. 


