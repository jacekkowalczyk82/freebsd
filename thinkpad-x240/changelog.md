# Change log 

## 2018-11-06 I started loggin all changes I made to my thinkpad setup to this change log file 

* git commit -am added system tweaks from RoboNuggie, https://www.youtube.com/watch?v=-k1-S6-aV9s , see git log for more details
* sudo pkg install ted scribus sterm 


 
* sudo pkg install xedit geany  
 
* sudo pkg install bsdstats
* added system tweaks for desktop use, see [config files](./thinkpad-x240/) 

* sudo pkg install dwm sterm 
* removed byobu, installed tmux 
* installed and configured DWM  
* installed suckless apps sterm, dmenu, slock surf-browser
* removed i3 
* Installed freeBSd 12.0-RELEASE
* fix for firefox issue with access to the graphics card: sudo chmod 777 /dev/dri/card0
* Fixed setup of Intel haswell drivers

```
sudo pkg install drm-kmod
sudo nano /etc/rc.conf
#and set 
kld_list="/boot/modules/i915kms.ko fuse"

``` 
