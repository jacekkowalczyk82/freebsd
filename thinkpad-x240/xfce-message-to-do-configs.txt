Message from xfce4-tumbler-0.2.5:

To override the default configuration, you must copy the rc-file:

        mkdir ~/.config/tumbler
        cp /usr/local/etc/xdg/tumbler/tumbler.rc ~/.config/tumbler

The COVER plugin requires manual configuration.

For more information see http://docs.xfce.org/xfce/thunar/tumbler
Message from xfce4-terminal-0.8.8:

Some options could need manual change to ~/.config/xfce4/terminal/terminalrc:

To reduce the height of tabs, add the hidden 'MiscSlimTabs' option:

MiscSlimTabs=TRUE

By default this option is not defined.

If you have configured a custom color cursor you will also need to add:

ColorCursorUseDefault=FALSE

to have such configuration still working, otherwise reconfigure the color
in the GUI.

Keep in mind, when you change an option in Preferences window, this file
is overwritten.
Message from xfce4-session-4.12.1_6:

To be able to shutdown or reboot your system, you'll have to add .rules
files in /usr/local/etc/polkit-1/rules.d directory. Which looks
like this (replace PUTYOURGROUPHERE by your group):

polkit.addRule(function (action, subject) {
  if ((action.id == "org.freedesktop.consolekit.system.restart" ||
      action.id == "org.freedesktop.consolekit.system.stop")
      && subject.isInGroup("PUTYOURGROUPHERE")) {
    return polkit.Result.YES;
  }
});

For those who have working suspend/resume:

polkit.addRule(function (action, subject) {
  if (action.id == "org.freedesktop.consolekit.system.suspend"
      && subject.isInGroup("PUTYOURGROUPHERE")) {
    return polkit.Result.YES;
  }
});
