#!/bin/sh 

tail -n 20 /var/log/messages

USB_BT_DEVICE=""

echo "==========================================="
while [ "$USB_BT_DEVICE" == "" ] ; do
echo "Select USB BT dongle device:"
echo -e "\t 0 - ubt0"
echo -e "\t 1 - ubt1"
echo -e "\t 2 - ubt2"
echo -e "\t q|Q - exit "
echo ""

read -p "Please select USB BT dongle device: " choice
case $choice in
  0) USB_BT_DEVICE=ubt0; break;;
  1) USB_BT_DEVICE=ubt1; break;;
  2) USB_BT_DEVICE=ubt2; break;;
  q|Q) exit 0;;
  *) echo "Invalid answer, please try again";;
#
esac
done

echo "==========================================="

HEADPHONES_DEVICE_NAME=""
AUDIO_OUT_DEVICE=dsp5

while [ "$HEADPHONES_DEVICE_NAME" == "" ] ; do
echo "Select configured HEADPHONES:"
echo -e "\t a|A - BT receiver BTR-302"
echo -e "\t b|B - FIIO uBTR"
echo -e "\t c|C - JBL-flip4-Szymona"
echo -e "\t q|Q - exit "
echo ""

read -p "Please select headsets to connect with : " choice
case $choice in
  a|A) HEADPHONES_DEVICE_NAME="BT-receiver-BTR-302"; break;;
  b|B) HEADPHONES_DEVICE_NAME="Fiio_uBTR"; break;;
  c|C) HEADPHONES_DEVICE_NAME="JBL-flip4-Szymona"; break;;
  q|Q) exit 0;;
  *) echo "Invalid answer, please try again";;
#
esac

done


# start Bt stack
#echo "service bluetooth start $USB_BT_DEVICE "
#sudo service bluetooth start $USB_BT_DEVICE 


#sudo hccontrol -n ubt1hci inquiry
#sudo hccontrol -n ubt1hci remote_name_request 
#sudo hccontrol -n ubt0hci write_authentication_enable 0



#connect to BT device , device should be already added to /etc/bluetooth/hosts
echo "hccontrol -n ${USB_BT_DEVICE}hci create_connection ${HEADPHONES_DEVICE_NAME} "
sudo hccontrol -n ${USB_BT_DEVICE}hci create_connection ${HEADPHONES_DEVICE_NAME} 
#sudo hccontrol -n ubt0hci create_connection Fiio_uBTR
#sudo hccontrol -n ubt0hci Read_Connection_List Fiio_uBTR

## Disable kernel's /dev/dsp
#sudo sysctl hw.snd.basename_clone=0

#create virtual audio device by proxy with bt device 
#and name this new audio device dsp5,
#there should be created system device file /dev/dsp5

echo "Creating virtual output device ${AUDIO_OUT_DEVICE}" 
echo "You can start mplayer like that:"
echo "   mplayer -ao oss:/dev/dsp5 video_file.mp4"

echo "virtual_oss -C 2 -c 2 -r 48000 -b 16 -s 768 -R /dev/null -P /dev/bluetooth/${HEADPHONES_DEVICE_NAME} -d ${AUDIO_OUT_DEVICE}"
sudo virtual_oss -C 2 -c 2 -r 48000 -b 16 -s 768 -R /dev/null -P /dev/bluetooth/${HEADPHONES_DEVICE_NAME} -d ${AUDIO_OUT_DEVICE}
#sudo virtual_oss -C 2 -c 2 -r 48000 -b 16 -s 768 -R /dev/null -P /dev/bluetooth/Fiio_uBTR -d dsp

# while this is running open a second terminal and link defoult audio output to this dsp5
# sudo ln -s /dev/dsp5 /dev/dsp
# I think this is not needed so it was commented out 


# you can start mplayer like that
# mplayer -ao oss:/dev/dsp5 TANGO-I-CASH-caly-film-full-HD-1080p-polski-lektor-720p.mp4


# At the end stop BT stack
# sudo service bluetooth stop ${USB_BT_DEVICE}
