#!/bin/sh 

tail -n 20 /var/log/messages

USB_BT_DEVICE=""

echo "==========================================="
while [ "$USB_BT_DEVICE" == "" ] ; do
echo "Select USB BT dongle device:"
echo -e "\t 0 - ubt0"
echo -e "\t 1 - ubt1"
echo -e "\t 2 - ubt2"
echo -e "\t q|Q - exit "
echo ""

read -p "Please select USB BT dongle device: " choice
case $choice in
  0) USB_BT_DEVICE=ubt0; break;;
  1) USB_BT_DEVICE=ubt1; break;;
  2) USB_BT_DEVICE=ubt2; break;;
  q|Q) exit 0;;
  *) echo "Invalid answer, please try again";;
#
esac
done

echo "==========================================="

# start Bt stack
echo "service bluetooth start $USB_BT_DEVICE "
retry=0
bt_started=0
while [ $retry -lt 2 ]; do 
	sudo service bluetooth start $USB_BT_DEVICE 
	exit_code=$?
	if [ $exit_code -eq 0 ]; then 
		bt_started=1
		break
	fi 
	let "retry=retry+1"
done 

if [ $bt_started -eq 0 ]; then 
	echo "Error failed to start BT stack at $USB_BT_DEVICE"
	read -p "Please press any key to exit " any_key
	exit 1
fi 


sudo hccontrol -n ${USB_BT_DEVICE}hci inquiry 

read -p "Please press any key to exit " any_key

exit 0 
